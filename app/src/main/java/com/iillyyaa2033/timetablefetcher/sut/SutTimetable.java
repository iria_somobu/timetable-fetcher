package com.iillyyaa2033.timetablefetcher.sut;

import com.iillyyaa2033.timetablefetcher.data.CalendarEvent;
import com.iillyyaa2033.timetablefetcher.data.DateBounds;
import com.iillyyaa2033.timetablefetcher.data.Timetable;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SutTimetable extends Timetable {

    public static final String KEY_REQUEST = "BONCH_RQ";

    private static final boolean DEBUG_CONNECT = false;
    private static final boolean DEBUG_PARSE = false;

    private static HashMap<String, Long> times = new HashMap<>();

    static { // Это захардкоженные начала семестров СПбГУТ
        times.put("205.1819/1", 1535922000000L);
        times.put("205.1819/2", 1549832400000L);
        times.put("205.1920/1", 1567371600000L);
    }

    static long hardcodedStart = 1549832400000L;
;
    private ArrayList<SutPair> parsedPairs;

    public SutTimetable(Map<String, String> params, Map<String, String> shortens){
        super(params, shortens);

        parsedPairs = new ArrayList<>();
    }

    public synchronized void prepare() throws IOException {

        // Clear old timetable
        parsedPairs.clear();

        if(DEBUG_CONNECT) v("Started...");

        String rq = getParams().get(SutTimetable.KEY_REQUEST);
        if(rq == null) rq = "https://cabinet.sut.ru/raspisanie_all_new?schet=205.1819%2F2&type_z=1&faculty=50005&kurs=1&group=53808&ok=%D0%9F%D0%BE%D0%BA%D0%B0%D0%B7%D0%B0%D1%82%D1%8C&group_el=0";

        Connection conn = Jsoup.connect(rq).userAgent("Mozilla/5.0").timeout(10 * 1000).method(Connection.Method.POST);

        String key = rq.substring(rq.indexOf("schet=") + "schet=".length());
        int idx = key.indexOf("&");
        key = key.substring(0, idx > 0 ? idx : key.length());
        key = key.replace("%2F","/");

        if(DEBUG_CONNECT) System.out.println("Got key "+key);
        hardcodedStart = times.get(key);

        Connection.Response response = conn.execute();
        if(DEBUG_CONNECT) v("Connected...");

        Document document = response.parse();
        Elements pairs = document.getElementsByClass("pair");

        // Parse result
        for(int i = 0; i < pairs.size(); i++) {
            Element el = pairs.get(i);
            if(DEBUG_PARSE) v(el.html());

            String weekday = el.attr("weekday");
            String pair = el.attr("pair");
            String subject = el.getElementsByClass("subect").get(0).text();
            String type = el.getElementsByClass("type").get(0).text();
            String weeks = el.getElementsByClass("weeks").get(0).text();

            Elements teacherEl = el.getElementsByClass("teacher");
            String teacher;
            if(teacherEl.size() > 0) {
                if(teacherEl.get(0).hasAttr("title")){
                    teacher = teacherEl.get(0).attr("title");
                } else {
                    teacher = teacherEl.get(0).text();
                }
            } else {
                teacher = "Препод не указан";
            }

            String aud = el.getElementsByClass("aud").get(0).text();
            parsedPairs.add(SutPair.parse(weekday, pair, subject, type, weeks, teacher, aud));
        }

        if (DEBUG_PARSE) {
            v("Parsed:");

            for (SutPair p : parsedPairs) {
                v(p.toString());
            }
        }
    }

    public ArrayList<CalendarEvent> getEventsAt(DateBounds bounds){
        ArrayList<CalendarEvent> events = new ArrayList<>();

        for(SutPair pair : parsedPairs){
            for(long start : pair.getAllStarts()){

                if(bounds.contains(start)){
                    events.add(
                        new CalendarEvent(-1,-1, pair.getTitle(getShortens()), start,
                            pair.getDuration(), pair.getDesctiption(), pair.getLocation()));
                }
            }
        }

        return events;
    }

    private void v(String s){
        System.out.println(s);
    }
}
