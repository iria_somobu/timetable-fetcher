package com.iillyyaa2033.timetablefetcher.eltech;

import java.util.ArrayList;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EltechPair {

    private static final long WEEK_LEN = 7 * 24 * 60 * 60 * 1000;

    static int TYPE_FULL = 0;
    static int TYPE_FIRST = 1;
    static int TYPE_SECOND = 2;

    /**
     * Начало пары, в миллисекундах относительно начала недели
     */
    public long startMillis;
    public long duration = (long) (1.5f * 60 * 60 * 1000);

    /**
     * Тип пары
     */
    public int type;

    public String title;
    public String description;
    public String location;

    public EltechPair(long startMillis, String title){
        this(startMillis, title, "");
    }

    public EltechPair(long startMillis, String title, String description){
        this(startMillis, TYPE_FULL, title, description);
    }

    public EltechPair(long startMillis, int type, String title, String descr){
        this.startMillis = startMillis;
        this.type = type;
        this.title = title.trim();
        description = descr.replace(".0","");

        Matcher m = Pattern.compile("\\d\\d\\d\\d").matcher(description);
        if(m.find()){
            String full = description.substring(m.start(), m.end());
            description = description.replace(full, "").trim();
            location = full;
        } else {
            location = "";
        }

        if(title.contains("с 10.40")){ // Грязный хак, ага
            this.startMillis += 50 * 60 * 1000;
            this.duration = 45 * 60 * 1000;
            this.title = title.replace("с 10.40", "").trim();
        }
    }


    @Override
    public String toString() {
        return title;
    }

    public Long[] getStarts(){
        ArrayList<Long> starts = new ArrayList<>();

        for(int w = 0; w < EltechTimetable.WEEK_COUNT; w++){
            long t = EltechTimetable.ZERO_WEEK_START + w * WEEK_LEN + startMillis;

            if(type == TYPE_FULL) starts.add(t);
            else if (type == TYPE_FIRST && w % 2 == 1) starts.add(t);
            else if (type == TYPE_SECOND && w % 2 == 0) starts.add(t);
        }

        return starts.toArray(new Long[0]);
    }

    public String getTitle(Map<String, String> shortens) {
        if(shortens.containsKey(title.toLowerCase())) return shortens.get(title.toLowerCase());

        System.out.println("Not shorten: "+title.toLowerCase());
        return title;
    }

    public long getDuration() {
        return duration;
    }

    public String getDesctiption() {
        return description;
    }

    public String getLocation() {
        return location;
    }
}
