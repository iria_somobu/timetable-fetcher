package com.iillyyaa2033.timetablefetcher.data;

public class CalendarInfo {

    public final String id;
    public final String name;

    public CalendarInfo(String id, String name){
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return name+" (#"+id+")";
    }
}
