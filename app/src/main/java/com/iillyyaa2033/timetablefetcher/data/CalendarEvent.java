package com.iillyyaa2033.timetablefetcher.data;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class CalendarEvent {

    public final long id;
    public final long calendarId;
    public final String title;
    public final long start;

    // Optional data
    public long duration = 0;
    public String description = "";
    public String location = "";

    public CalendarEvent(long id, long calendar, String title, long start) {
        this.title = title;
        this.start = start;
        this.calendarId = calendar;
        this.id = id;
    }

    public CalendarEvent(long id, long calendar, String title, long start,
                         long duration, String description, String location){
        this(id, calendar, title, start);
        this.duration = duration;
        this.description = description;
        this.location = location;
    }

    @Override
    public String toString() {
        String formattedStart = new SimpleDateFormat("yyyy.MM.dd hh:mm", Locale.getDefault()).format(new Date(start));
        return "#"+id+" "+title +" "+formattedStart;
    }

    public JSONObject toJson(){
        JSONObject result = new JSONObject();

        try {
            result.put("id",id);
            result.put("calendarId",calendarId);
            result.put("title",title);
            result.put("start",start);
            result.put("duration",duration);
            result.put("description",description);
            result.put("location",location);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return result;
    }

    public boolean atSameDate(CalendarEvent event){
        return event.start == start;
    }
}
