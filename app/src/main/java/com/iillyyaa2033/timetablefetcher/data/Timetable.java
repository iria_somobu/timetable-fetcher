package com.iillyyaa2033.timetablefetcher.data;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

public abstract class Timetable {

    private Map<String, String> params;
    private Map<String, String> shortens;

    public Timetable(Map<String, String> params, Map<String, String> shortens){
        this.params = params;
        this.shortens = shortens;
    }

    public Map<String, String> getParams(){
        return params;
    }

    public Map<String, String> getShortens(){
        return shortens;
    }

    public abstract void prepare() throws Exception;

    public abstract ArrayList<CalendarEvent> getEventsAt(DateBounds bounds);
}
